var app = angular.module('App', ['ngRoute']);

app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'app/views/home.html',
            controller: 'Home'
        })
        .when('/movies/form', {
            templateUrl: 'app/views/form/movie.html',
            controller: 'MovieForm'
        })
        .when('/movies/list', {
            templateUrl: 'app/views/list/movie.html',
            controller: 'MovieList'
        })
        .when('/movies/:movie_id/display', {
          templateUrl: 'app/views/display/movie.html',
          controller: 'MovieDisplay'
        })
        .when('/series/form', {
            templateUrl: 'app/views/form/series.html',
            controller: 'SeriesForm'
        })
        .when('/series/list', {
            templateUrl: 'app/views/list/series.html',
            controller: 'SeriesList'
        })
        .when('/series/:series_id/seasons/list', {
            templateUrl: 'app/views/list/season.html',
            controller: 'SeasonList'
        })
        .when('/series/:series_id/seasons/:season_id/episodes/list', {
          templateUrl: 'app/views/list/episode.html',
          controller: 'EpisodeList'
        })
        .when('/series/:series_id/seasons/:season_id/episodes/:episode_id/display', {
          templateUrl: 'app/views/display/episode.html',
          controller: 'EpisodeDisplay'
        });
}]);
