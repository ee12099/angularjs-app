app.service('$api', function ($http) {

  /* MOVIE */

  var movie = {};

  var setMovie = function (object) {
    movie = object;
  };

  var getMovie = function () {
    return movie;
  };

  var retrieveAllMovies = function (callback) {
    $http.get('http://localhost:5000/api/movies')
    .then(function (response) {
      console.log(response.data);
      callback(response.data);
    });
  };

  var movie_struct = {
    list: retrieveAllMovies,
    set: setMovie,
    get: getMovie
  };

  /* SERIES */

  var series = {};

  var setSeries = function (object) {
    series = object;
  };

  var getSeries = function () {
    return series;
  };

  var retrieveAllSeries = function (callback) {
    $http.get('http://localhost:5000/api/series')
      .then(function (response) {
        console.log(response.data);
        callback(response.data);
      });
  };

  var series_struct = {
    list: retrieveAllSeries,
    set: setSeries,
    get: getSeries
  };

  /* SEASON */

  var season = {};

  var setSeason = function (object) {
    season = object;
  };

  var getSeason = function () {
    return season;
  };

  var season_struct = {
    set: setSeason,
    get: getSeason
  };

  /* EPISODE */

  var episode = {};

  var setEpisode = function (object) {
    episode = object;
  };

  var getEpisode = function () {
    return episode;
  };

  var episode_struct = {
    set: setEpisode,
    get: getEpisode
  };


  return {
    movie: movie_struct,
    series: series_struct,
    season: season_struct,
    episode: episode_struct
  }
});
