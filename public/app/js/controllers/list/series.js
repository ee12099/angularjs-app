app.controller('SeriesList', ['$scope', '$window', '$api', function ($scope, $window, $api) {
  $scope.Series = {};
  $scope.Series.init = function () {
      $api.series.list(function (series) {
        $scope.Series.list = series;
      });
  };
  $scope.Series.select = function (series) {
      $api.series.set(series);
      $window.location = '#!/series/' + series._id + '/seasons/list';
  };

}]);
