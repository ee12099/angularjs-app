app.controller('EpisodeList', ['$scope', '$window', '$api', function ($scope, $window, $api) {
  $scope.Episode = {};
  $scope.Episode.series = $api.series.get();
  $scope.Episode.season = $api.season.get();
  $scope.Episode.init = function () {
    console.log('Episode Init');
    $scope.Episode.list = $scope.Episode.season.episodes;
  };
  $scope.Episode.select = function (episode) {
    $api.episode.set(episode);
    console.log(episode);
    $window.location = '#!/series/'+$scope.Episode.series._id+'/seasons/' +$scope.Episode.season._id+ '/episodes/'+episode._id+'/display';
  }
}]);
