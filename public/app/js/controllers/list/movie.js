app.controller('MovieList', ['$scope', '$window', '$api', function ($scope, $window, $api) {
    $scope.Movie = {};
    $scope.Movie.init = function () {
      $api.movie.list(function (movies) {
        console.log(movies);
        $scope.Movie.list = movies;
      });
    };
    $scope.Movie.select = function (movie) {
        $api.movie.set(movie);
        $window.location = '#!/movies/' + movie._id + '/display';
    }
}]);
