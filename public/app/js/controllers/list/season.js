app.controller('SeasonList', ['$scope', '$window', '$api', function ($scope, $window, $api) {
  $scope.Season = {};
  $scope.Season.series = $api.series.get();
  $scope.Season.init = function () {
    console.log('Season Init');
    $scope.Season.list = $scope.Season.series.seasons;
  };
  $scope.Season.select = function (season) {
    $api.season.set(season);
    console.log(season);
    $window.location = '#!series/'+ $scope.Season.series._id + '/seasons/' + season._id + '/episodes/list'
  }
}]);
