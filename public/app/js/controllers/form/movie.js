app.controller('MovieForm', ['$scope', '$http', function ($scope, $http) {
    $scope.video = {};
    $scope.movie = {};

    $scope.postVideo = function () {

        var data = new FormData();
        data.append('filename', $scope.file.name);
        data.append('video', $scope.file);

        $http.post('/api/videos', data, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function (response) {
            $scope.video = response.data;
        });
    }

    $scope.createMovie = function (movie) {
      postVideo().then(function () {
        $http.post('/api/movies', movie, {
          transformRequest: angular.identity,
          headers: { 'Content-Type': undefined }
        }).then(function (response) {
          $scope.movie = response.data;
        })
      })
    }
}]);
