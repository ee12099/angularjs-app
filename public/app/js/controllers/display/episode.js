app.controller('EpisodeDisplay', ['$scope', '$api',
function ($scope, $api) {
  $scope.series = $api.series.get();
  $scope.season = $api.season.get();
  $scope.episode = $api.episode.get();
  console.log($scope.episode);
}]);
