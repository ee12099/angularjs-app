FROM node:argon

RUN useradd -ms /bin/bash bower

RUN mkdir /app
WORKDIR /app

COPY package.json /app
RUN npm install

RUN npm install --global bower

COPY . /app

WORKDIR /app/public/assets
USER bower
RUN bower install

EXPOSE 3000

CMD ["npm", "start"]