var express = require('express');
var path = require('path');

var app = express();

const PORT = process.env.PORT || 3000;

app.use(express.static('public'));

app.get('/', function (request, response) {
    response.sendFile(path.join(__dirname + '/public/index.html'));
});

app.listen(PORT, function () {
    console.log(`App listening on port ${PORT}`);
});